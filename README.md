# @revas-io/platform-web

> Revas Platform SSR website

## Development Setup

Create a `.env` file on root directory with the following content: 

```
INTERCOM_ID=XXXXXX
ANALYTICS_ID=UA-XXXXXX-X
GOOGLE_SITE_VERIFICATION=XXXX
PIXEL_ID=XXX
APP_URL=http://127.0.0.1:3000
REVAS_PUBLIC_API_URL=XXX
REVAS_ORGANIZATION_ID=XXXXX
```

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
