'use strict'

const memcache = require('memory-cache')

module.exports = {
  createMiddleware (options) {
    const cache = new memcache.Cache()
    return (req, res, next) => {
      const key = '__express__' + req.originalUrl || req.url
      const cached = cache.get(key)
      if (cached) {
        res.send(cached)
      } else {
        res.$send = res.send
        res.send = (body) => {
          if (res.statusCode === 200) {
            cache.put(key, body, options.expiresIn)
          }
          res.$send(body)
        }
        next()
      }
    }
  }
}
