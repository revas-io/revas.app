'use strict'

var fs = require('fs')
const axios = require('axios')
const _ = require('lodash')

require.extensions['.md'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8')
}

/* eslint-disable vue/script-indent */
const express = require('express')
const {Nuxt, Builder} = require('nuxt')
const marked = require('marked')

const app = express()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

app.set('port', port)

// const revas = require('../revas.config.js')
// const BlogService = require('./blog')
// const cache = require('./cache')

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.env.HOST = host
config.env.PORT = port
config.dev = !(process.env.NODE_ENV === 'production')

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)
  // const blog = new BlogService()

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // app.get('/api/posts/:lang',
  //   cache.createMiddleware(revas.cache),
  //   async (req, res) => {
  //     try {
  //       let posts = await blog.getPostsAsync(req.params.lang)
  //       posts.reverse()
  //       res.json(posts)
  //     } catch (e) {
  //       console.log(e.Error)
  //       res.sendStatus(500)
  //     }
  //   })

  // app.get('/api/posts/:lang/:slug',
  //   cache.createMiddleware(revas.cache),
  //   async (req, res) => {
  //     try {
  //       const post = await blog.getPostAsync(req.params.lang, req.params.slug)
  //       if (post) {
  //         res.json(post)
  //       } else {
  //         res.sendStatus(404)
  //       }
  //     } catch (e) {
  //       console.log(e.Error ? e.Error : e)
  //       res.sendStatus(500)
  //     }
  //   })

  const getPosts = async () => {
    const { data } = await axios.post(process.env.REVAS_PUBLIC_API_URL + '/publiko.QueryPosts/', {
      organizations: [{
        id: process.env.REVAS_ORGANIZATION_ID,
        blogs: [{
          domain: 'default'
        }]
      }]
    })
    console.log('data', data)
    return _.get(data, ['organizations', '0', 'blogs', '0', 'posts'])
  }

  app.get('/api/posts', async (req, res) => {
    let body = {}
    try {
      body.posts = await getPosts()
      console.log(body.posts)
    } catch (e) {
    }
    res.send(body)
  })

  app.get('/api/posts/:slug', async (req, res) => {
    let body = {}
    try {
      const posts = await getPosts()
      const post = _.find(posts, (p) => {
        return p.slug === req.params.slug
      })
      const { data } = await axios.post(process.env.REVAS_PUBLIC_API_URL + '/publiko.GetPosts/', {
        organizations: [{
          id: process.env.REVAS_ORGANIZATION_ID,
          blogs: [{
            domain: 'default',
            posts: [{
              id: post.id
            }]
          }]
        }]
      })
      let content = _.get(data, ['organizations', '0', 'blogs', '0', 'posts', '0', 'content'])
      post.content = marked(content, { sanitize: true })
      body.post = post
    } catch (e) {
      body.error = e.toString()
    }
    res.send(body)
  })

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  console.log('Server listening on http://' + host + ':' + port) // eslint-disable-line no-console
}

start()
