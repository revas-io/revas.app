import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      locales: ['it-IT'],
      locale: 'it-IT'
    },
    mutations: {
      SET_LANG (state, locale) {
        if (state.locales.indexOf(locale) !== -1) {
          state.locale = locale
        }
      }
    },
    modules: {
      privacy: {
        namespaced: true,
        state: {
          consent: null,
          cookies: []
        },
        getters: {
          hasConsent (state) {
            return state.consent
          },
          cookies (state) {
            return state.cookies
          }
        },
        mutations: {
          consent (state, cookies) {
            state.consent = new Date()
            state.cookies = cookies
          },
          revoke (state) {
            state.consent = null
            state.cookies = []
            document.cookie.split(';').forEach((c) => {
              document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/')
            })
          }
        }
      }
    }
  })
}

export default createStore
