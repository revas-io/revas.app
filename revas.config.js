module.exports = {
  cache: {
    expiresIn: 1000 * 60 * 60 * 24
  },
  blog: {
    gitlab: {
      project: 7303058,
      path: 'posts'
    }
  }
}
