import Vue from 'vue'

import VueIntercom from 'vue-intercom/src'

export default ({ app, env }, inject) => {
  Vue.use(VueIntercom, { appId: env.INTERCOM_ID })
  inject('intercomCookie', new Vue({
    methods: {
      consent () {
        this.$intercom.boot()
      },
      revoke () {
        if (this.$intercom) {
          this.$intercom.shutdown()
        }
      }
    }
  }))
}
