import Vue from 'vue'

import VueAnalytics from 'vue-analytics'

export default ({ app, env }, inject) => {
  Vue.use(VueAnalytics, {
    id: env.ANALYTICS_ID,
    disabled: true,
    router: app.router
  })
  inject('analyticsCookie', new Vue({
    methods: {
      consent () {
        this.$ga.enable()
      },
      revoke () {
        this.$ga.disable()
      }
    }
  }))
}
