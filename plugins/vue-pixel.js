import Vue from 'vue'
import VueFacebookPixel from 'vue-analytics-facebook-pixel'

export default ({ app, env }, inject) => {
  Vue.use(VueFacebookPixel)
  inject('pixelCookie', new Vue({
    methods: {
      consent () {
        debugger
        this.$analytics.fbq.init(env.PIXEL_ID)
        debugger
        this.$analytics.fbq.event('PageView')
      },
      revoke () {
      }
    }
  }))
}
