require ('dotenv').config()

module.exports = {
  mode: 'universal',
  env: {
    INTERCOM_ID: process.env.VUE_APP_INTERCOM_ID,
    ANALYTICS_ID: process.env.VUE_APP_ANALYTICS_ID,
    PIXEL_ID: process.env.VUE_APP_PIXEL_ID,
    APP_URL: process.env.APP_URL
  },
  head: {
    title: 'Revas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Il tuo tempo per ciò che conta davvero' },
      { name: 'google-site-verification', content: process.env.VUE_APP_GOOGLE_SITE_VERIFICATION }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ],
    script: [
      { src: '/fbevents.js' }
    ]
  },
  loading: { color: '#FFFFFF' },
  router: {
    middleware: 'vue-i18n'
  },
  plugins: [
    '~/plugins/vue-i18n.js',
    { src: '~/plugins/vuex-storage.js', ssr: false },
    { src: '~/plugins/vue-intercom.js', ssr: false },
    { src: '~/plugins/vue-analytics.js', ssr: false },
    { src: '~/plugins/vue-pixel.js', ssr: false }
  ],
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    [
      'nuxt-fontawesome', {
        component: 'fa-icon',
        imports: [
          {
            set: '@fortawesome/fontawesome-free-solid'
          },
          {
            set: '@fortawesome/fontawesome-free-brands'
          }
        ]
      }
    ]
  ],
  fontAwesome: {
    packs: [
      {
        package: '@fortawesome/fontawesome-free-solid'
      },
      {
        package: '@fortawesome/fontawesome-free-brands'
      }
    ]
  },
  axios: {
  },
  build: {
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
